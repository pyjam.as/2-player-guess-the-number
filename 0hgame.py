import os

os.system("clear")
print("Input between 0 and 100")
p1 = int(input("player1> "))


os.system("clear")
print("Input between 0 and 100")
p2 = int(input("player2> "))
os.system("clear")


player1_turn = True

while True:
    if player1_turn:
        n = int(input("Make your guess player1: "))
        if n == p2:
            print("Player1 wins!")
            exit(0)
        else:
            os.system("clear")
            n1, n2 = abs(p1 - n), abs(p2 - n)
            if n1 < n2:
                print("You are closer to yourself")
            else:
                print("You are closer to your opponent")
        player1_turn = False

    else:
        n = int(input("Make your guess player2: "))
        if n == p1:
            print("Player2 wins!")
            exit(0)
        else:
            os.system("clear")
            n1, n2 = abs(p1 - n), abs(p2 - n)
            if n2 < n1:
                print("You are closer to yourself")
            else:
                print("You are closer to your opponent")
        player1_turn = True
